obj-m	:= ataLib.o

KDIR	:= /lib/modules/$(shell uname -r)/build
PWD	:= $(shell pwd)

default:
	make -C $(KDIR) SUBDIRS=$(PWD) modules

clean:
	rm modules.* Module.* *.ko *.o *.mod.c
