#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>    

// ATA Protocols
#define HARD_RESET 		    0
#define SRST	   		    1
#define RESERVED       		    2
#define NON_DATA   		    3
#define PIO_DATA_IN 		    4
#define PIO_DATA_OUT     	    5
#define DMA           		    6
#define DMA_QUEUED   		    7
#define DEVICE_DIAGNOSTIC 	    8
#define DEVICE_RESET 		    9
#define UDMA_DATA_IN 		    10
#define UDMA_DATA_OUT 	            11
#define FPDMA                       12
#define RETURN_RESPONSE_INFORMATION 15

struct commandDescriptorBlock
{
	int opCode:8;        // Operation code 
	int reserved:1;      // Reserved
	int protocol:4;      // Protocol
	int multiCount:3;
	int tLength:2;
	int byteBlok:1;
	int tDir:1;
	int reserved2:1;
	int ckCond:1;
	int offline:2;
	int features:8;
	int sectorCount:8;
	int lbaLow:8;
	int lbaMid:8;
	int lbaHigh:8;
	int device:8;
	int command:8;
	int reserved:8;
	int control:8;
};


/*
This marks the beginning of the module
*/
static int __init main(void)
{
    printk(KERN_INFO "Initiating custom ata library\n");
    
    return 0;
}

/*
This is where the module exits and cleans stuff up
*/
static void __exit hello_end(void)
{

    printk(KERN_INFO "Destroying custom ata library\n");
}

module_init(main);  // Macro that allows the user to use whatever name they want for the init function
module_exit(hello_end);   // Macro that allows the user to use whatever name they want for the exit function

MODULE_LICENSE("GPL");           // Declares the code as GPL to get rid of that licensing warning during compilation  
MODULE_AUTHOR("Maurice Gale");   // Declares the author of the module
MODULE_DESCRIPTION("Variables"); //Gives a description to the module

